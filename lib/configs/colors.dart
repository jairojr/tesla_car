import 'package:flutter/material.dart';

const Color kBottomAppBarColor = Color.fromARGB(255, 49, 52, 63);
const Color kPrimaryColor = Color.fromARGB(255, 1, 161, 254);
const Color kSecondaryColor = Color.fromARGB(255, 107, 200, 252);

const LinearGradient buttonGradient = LinearGradient(
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
  colors: [kPrimaryColor, kSecondaryColor],
);
