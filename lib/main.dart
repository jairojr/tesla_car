import 'package:flutter/material.dart';
import 'package:tesla_car/screens/base_screens.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        iconTheme:IconThemeData(color: Color.fromARGB(255, 101, 101, 101)),
        textTheme:TextTheme(
          bodyText2:TextStyle(
            color:Colors.white,
          ),
        ),
      ),
      home: BaseScreen(),
    );
  }
}
